﻿namespace WebApi.Dtos
{
    public class CustomerDto
    {
        public CustomerDto()
        {

        }

        public CustomerDto(int id, string fullName, string email, string phone)
        {
            Id = id;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }

        /// <summary>
        /// Получает или задает идентификатор контакта.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает ФИО контакта.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Получает или задает адрес электронной почты контакта.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Получает или задает номер телефона контакта.
        /// </summary>
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"Id = '{Id}' FullName = '{FullName}' Email = '{Email}' Phone = '{Phone}'";
        }
    }
}
