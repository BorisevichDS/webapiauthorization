﻿namespace WebApi.Dtos
{
    public class TokenDto
    {
        public string Token { get; set; }
    }
}
