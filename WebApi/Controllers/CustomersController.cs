﻿using Core.Entities;
using Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using WebApi.Dtos;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IService<Customer, int> service;

        public CustomersController(IService<Customer, int> service)
        {
            this.service = service;
        }

        /// <summary>
        /// Метод ищет контакта по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns>Результат выполнения запроса, включая данные контакта.</returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<CustomerDto>> Get(int id)
        {
            try
            {
                Customer customer = await this.service.GetAsync(id);

                if (customer == null)
                {
                    return new StatusCodeResult((int)HttpStatusCode.NotFound);
                }
                else
                {
                    CustomerDto customerDto = new CustomerDto(customer.Id, customer.FullName, customer.Email, customer.Phone);
                    return Ok(customerDto);
                }
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Метод создает контакта.
        /// </summary>
        /// <param name="customer">Данные контакта.</param>
        /// <returns>Результат выполнения запроса.</returns>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CustomerDto customerDto)
        {
            try
            {
                Customer existingCustomer = await this.service.GetAsync(customerDto.Id);

                if (existingCustomer == null)
                {
                    Customer customer = new Customer(customerDto.Id, customerDto.FullName, customerDto.Email, customerDto.Phone);
                    await this.service.SaveAsync(customer);
                    return new StatusCodeResult((int)HttpStatusCode.OK);
                }
                else
                {
                    return new StatusCodeResult((int)HttpStatusCode.Conflict);
                }
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}