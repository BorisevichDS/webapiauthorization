﻿using Core.Entities;
using Core.Services;
using Core.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Dtos;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IEncodingKey encodingKey;
        private readonly IService<User, string> service;

        public AuthenticationController(IEncodingKey encodingKey, IService<User, string> service)
        {
            this.encodingKey = encodingKey;
            this.service = service;
        }

        /// <summary>
        /// Зарегистрировать пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Токен.</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<TokenDto>> Post([FromBody] UserDto user)
        {
            try
            {
                User existingUser = await this.service.GetAsync(user.Login);

                if (existingUser == null)
                {
                    await this.service.SaveAsync(new User(user.Login, user.Password));

                    var claims = new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Login),
                    };

                    JwtSecurityToken token = new JwtSecurityToken(
                        issuer: "DMIB",
                        audience: "WebApiAuthenticationDemo",
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(5),
                        signingCredentials: new SigningCredentials(this.encodingKey.SymmetricKey, this.encodingKey.Algorithm)
                        );

                    TokenDto tokenDto = new TokenDto();
                    tokenDto.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    return Ok(tokenDto);
                }
                else
                {
                    return new StatusCodeResult((int)HttpStatusCode.Conflict);
                }
            }
            catch(Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}