using Core.Entities;
using Core.Repositories;
using Core.Services;
using DataAccess.Contexts;
using DataAccess.Repositories;
using DataAccess.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Core.Tokens;
using Core.Hashers;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddJsonOptions(o => o.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddDbContext<WebApiDbContext>(option => option.UseSqlite(this.Configuration.GetConnectionString("Sample")));
            
            services.AddSingleton<ICacheService<Customer, int>, CustomerCacheService>();

            services.AddScoped<IRepository<Customer, int>, CustomersRepository>();
            services.AddScoped<IRepository<User, string>, UsersRepository>();

            services.AddSingleton<IEncodingKey, EncodingKey>();
            services.AddSingleton<IHasher, Sha256Hasher>();

            services.AddScoped<IService<Customer, int>, CustomersService>();
            services.AddScoped<IService<User, string>, UsersService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
