﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Contexts
{
    public class WebApiDbContext : DbContext
    {
        public WebApiDbContext(DbContextOptions<WebApiDbContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var customerEntity = modelBuilder.Entity<Customer>();

            customerEntity.ToTable("Customer");
            customerEntity.HasKey(k => k.Id).HasName("id");

            customerEntity.Property(p => p.Id).ValueGeneratedNever();
            customerEntity.Property(p => p.Email).HasColumnName("email").HasMaxLength(100);
            customerEntity.Property(p => p.Phone).HasColumnName("phone").HasMaxLength(50);
            customerEntity.Property(p => p.FullName).HasColumnName("fullname").HasMaxLength(200);

            var userEntity = modelBuilder.Entity<User>();

            userEntity.ToTable("User");
            userEntity.HasKey(k => k.Id).HasName("id");

            userEntity.Property(p => p.Id).HasColumnName("id").ValueGeneratedOnAdd();
            userEntity.Property(p => p.Login).HasColumnName("login");
            userEntity.Property(p => p.Password).HasColumnName("password_hash");
            userEntity.Property(p => p.CreatedOn).HasColumnName("created_on").ValueGeneratedOnAdd();
        }
    }
}
