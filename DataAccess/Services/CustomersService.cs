﻿using Core.Entities;
using Core.Repositories;
using Core.Services;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    public class CustomersService : IService<Customer, int>
    {
        private readonly IRepository<Customer, int> repository;
        private readonly ICacheService<Customer, int> cacheService;

        public CustomersService(IRepository<Customer, int> repository, ICacheService<Customer, int> cacheService)
        {
            this.repository = repository;
            this.cacheService = cacheService;
        }

        /// <summary>
        /// Получить данные контакта по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns>Контакт.</returns>
        public async Task<Customer> GetAsync(int id)
        {
            Customer customer;
            if (!this.cacheService.TryGetValue(id, out customer))
            {
                customer = await this.repository.FindAsync(id);

                if (customer != null)
                {
                    await this.cacheService.AddAsync(id, customer);
                }
            }

            return customer;
        }

        /// <summary>
        /// Зарегистрировать контакт.
        /// </summary>
        /// <param name="customer">Контакт.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task SaveAsync(Customer customer)
        {
            await this.repository.AddAsync(customer);
            await this.cacheService.AddAsync(customer.Id, customer);
        }
    }
}
