﻿using Core.Entities;
using Core.Hashers;
using Core.Repositories;
using Core.Services;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    /// <summary>
    /// Сервис для работы с данными пользователей.
    /// </summary>
    public class UsersService : IService<User, string>
    {
        private readonly IRepository<User, string> repository;
        private readonly IHasher hasher;

        public UsersService(IRepository<User, string> repository, IHasher hasher)
        {
            this.repository = repository;
            this.hasher = hasher;
        }

        /// <summary>
        /// Получить данные пользователя по логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public async Task<User> GetAsync(string login)
        {
            return await this.repository.FindAsync(login);
        }

        /// <summary>
        /// Cохранить данные пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task SaveAsync(User user)
        {
            user.Password = this.hasher.GetHash(user.Password);
            await this.repository.AddAsync(user);
        }
    }
}
