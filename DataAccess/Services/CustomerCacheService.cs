﻿using Core.Services;
using Core.Entities;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Services
{
    public class CustomerCacheService : ICacheService<Customer, int>
    {
        private Dictionary<int, Customer> cache;

        public CustomerCacheService()
        {
            this.cache = new Dictionary<int, Customer>();
        }

        /// <summary>
        /// Добавить данные контакта в кэш.
        /// </summary>
        /// <param name="key">Идентификатор контакта.</param>
        /// <param name="data">Данные контакта.</param>
        public async Task AddAsync(int key, Customer data)
        {
            await Task.Run(() => this.cache.Add(key, data));
        }

        /// <summary>
        /// Получить данные контакта из кэша.
        /// </summary>
        /// <param name="key">Идентификатор контакта.</param>
        /// <param name="data">Данные контакта.</param>
        /// <returns>Признак, показывающий успешно или нет прошел поиск.</returns>
        public bool TryGetValue(int key, out Customer data)
        {
            data = cache.Where(c => c.Key == key).FirstOrDefault().Value;
            return data != null ? true : false;
        }
    }
}
