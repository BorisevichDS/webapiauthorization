using Core.Entities;
using Core.Repositories;
using DataAccess.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий для взаимодействия с данными контактов.
    /// </summary>
    public class CustomersRepository: IRepository<Customer, int>
    {
        private readonly WebApiDbContext context;
        static object locker = new object();

        public CustomersRepository(WebApiDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Добавить одного контакта в базу данных.
        /// </summary>
        /// <param name="customer">Контакт.</param>
        public void Add(Customer customer)
        {
            if (this.context.Customers.Any(c => c.Equals(customer) == false))
            {
                this.context.Customers.Add(customer);
                this.context.SaveChanges();
            }
        }

        /// <summary>
        /// Добавить одного контакта в базу данных.
        /// </summary>
        /// <param name="customer">Контакт.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task AddAsync(Customer customer)
        {
            await this.context.Customers.AddAsync(customer);
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Добавить список контактов в базу данных.
        /// </summary>
        /// <param name="customers">Список контактов.</param>
        public void AddRange(List<Customer> customers)
        {
            List<Customer> customersToInsert = customers.Except(context.Customers).ToList();

            if (customersToInsert.Count > 0)
            {
               this.context.AddRange(customersToInsert.ToArray());
               this.context.SaveChanges();
            }
        }

        /// <summary>
        /// Добавить список контактов в базу данных.
        /// </summary>
        /// <param name="customers">Список контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task AddRangeAsync(List<Customer> customers)
        {
            await this.context.AddRangeAsync(customers.ToArray());
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Найти контакт по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns>Контакт.</returns>
        public async Task<Customer> FindAsync(int id)
        {
            return await this.context.Customers.FindAsync(id);
        }

        /// <summary>
        /// Найти контакт по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns>Контакт.</returns>
        public Customer Find(int id)
        {
            return this.context.Customers.Find(id);
        }
    }
}