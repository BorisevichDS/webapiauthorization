﻿using Core.Entities;
using Core.Repositories;
using DataAccess.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий для взаимодействия с данными пользователей.
    /// </summary>
    public class UsersRepository : IRepository<User, string>
    {
        private readonly WebApiDbContext context;

        public UsersRepository(WebApiDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Добавить одного пользователя в базу данных.
        /// </summary>
        /// <param name="user">Контакт.</param>
        public void Add(User user)
        {
            this.context.Users.Add(user);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Добавить одного пользователя в базу данных.
        /// </summary>
        /// <param name="user">Контакт.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task AddAsync(User user)
        {
            await this.context.Users.AddAsync(user);
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Добавить список пользователей в базу данных.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        public void AddRange(List<User> users)
        {
            this.context.Users.AddRange(users);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Добавить список пользователей в базу данных.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task AddRangeAsync(List<User> users)
        {
            await this.context.Users.AddRangeAsync(users);
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Найти пользователя по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public User Find(string login)
        {
            return this.context.Users.FirstOrDefault(u => u.Login.Equals(login));
        }

        /// <summary>
        /// Найти пользователя по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public async Task<User> FindAsync(string login)
        {
            return await Task.Run(() => this.context.Users.FirstOrDefault(u => u.Login.Equals(login)));
        }
    }
}
