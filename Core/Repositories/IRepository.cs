using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Repositories
{
    /// <summary>
    /// ����������� ��� �������������� � ������� ��������.
    /// </summary>
    public interface IRepository<TData, TKey>
    {
        /// <summary>
        /// �������� ������ �������� � ���������.
        /// </summary>
        /// <param name="data">�������.</param>
        void Add(TData data);

        /// <summary>
        /// �������� ������ �������� � ���������.
        /// </summary>
        /// <param name="data">������ ��������.</param>
        /// <returns>����������� ��������.</returns>
        Task AddAsync(TData data);

        /// <summary>
        /// �������� ������ ��������� � ���������.
        /// </summary>
        /// <param name="datas">������ ���������.</param>
        void AddRange(List<TData> datas);

        /// <summary>
        /// �������� ������ ��������� � ���������.
        /// </summary>
        /// <param name="datas">������ ���������.</param>
        /// <returns>����������� ��������.</returns>
        Task AddRangeAsync(List<TData> datas);

        /// <summary>
        /// ����� �������� �� � �����.
        /// </summary>
        /// <param name="key">���� ��������, ������� � ���������� ����������.</param>
        /// <returns>�������.</returns>
        TData Find(TKey key);

        /// <summary>
        /// ����� �������� �� � �����.
        /// </summary>
        /// <param name="key">���� ��������, ������� � ���������� ����������.</param>
        /// <returns>�������.</returns>
        Task<TData> FindAsync(TKey key);
    }
}