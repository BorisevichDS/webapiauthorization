﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        public User()
        {
            this.CreatedOn = DateTime.Now;
        }

        public User(string login, string password)
        {
            this.Login = login;
            this.Password = password;
            this.CreatedOn = DateTime.Now;
        }

        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
