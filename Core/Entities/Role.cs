﻿namespace Core.Entities
{
    public enum Role
    {
        Administrator,
        Reader
    }
}
