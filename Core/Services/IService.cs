﻿using System.Threading.Tasks;

namespace Core.Services
{
    public interface IService<TData, TKey>
    {       
        /// <summary>
        /// Получить данные по ключу.
        /// </summary>
        /// <param name="key">Ключ, который однозначно определяет сущность.</param>
        /// <returns>Данные сущности.</returns>
        Task<TData> GetAsync(TKey key);

        /// <summary>
        /// Cохранить данные сущности.
        /// </summary>
        /// <param name="data">Данные сущности.</param>
        /// <returns>Асинхронная операция.</returns>
        Task SaveAsync(TData data);
    }
}