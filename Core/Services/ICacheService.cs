﻿using System.Threading.Tasks;

namespace Core.Services
{
    /// <summary>
    /// Сервис для работы с кэшем.
    /// </summary>
    public interface ICacheService<TData, TKey>
    {
        /// <summary>
        /// Добавить данные в кэш.
        /// </summary>
        /// <param name="key">Идентификатор записи в кэше.</param>
        /// <param name="data">Данные.</param>
        public Task AddAsync(TKey key, TData data);

        /// <summary>
        /// Получить данные из кэша.
        /// </summary>
        /// <param name="key">Идентификатор записи в кэше.</param>
        /// <param name="data">Данные.</param>
        /// <returns>Признак, показывающий успешно или нет прошел поиск.</returns>
        public bool TryGetValue(TKey key, out TData data);
    }
}
