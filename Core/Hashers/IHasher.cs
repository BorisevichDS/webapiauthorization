﻿using System.Security.Cryptography;

namespace Core.Hashers
{
    /// <summary>
    /// Хешер.
    /// </summary>
    public interface IHasher
    {
        /// <summary>
        /// Получить хеш.
        /// </summary>
        /// <param name="input">Строка, для которой необходимо посчитать хеш.</param>
        /// <returns>Хеш.</returns>
        string GetHash(string input);

        /// <summary>
        /// Проверить хеш.
        /// </summary>
        /// <param name="input">Строка, для которой необходимо посчитать хеш.</param>
        /// <param name="hash">Хеш для сравнения.</param>
        /// <returns>Признак, который показывает соответствует ли хеш строки с переданным хешем.</returns>
        bool VerifyHash(string input, string hash);
    }
}
