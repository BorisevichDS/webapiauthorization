﻿using Microsoft.IdentityModel.Tokens;

namespace Core.Tokens
{
    /// <summary>
    /// Ключ для создания цифровой подписи.
    /// </summary>
    public interface IEncodingKey
    {
        /// <summary>
        /// Алгоритм шифирования.
        /// </summary>
        string Algorithm { get; }

        /// <summary>
        /// Ключ цифровой подписи.
        /// </summary>
        SymmetricSecurityKey SymmetricKey { get; } 
    }
}
