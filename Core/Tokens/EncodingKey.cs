﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Core.Tokens
{
    /// <summary>
    /// Ключ для создания цифровой подписи.
    /// </summary>
    public class EncodingKey : IEncodingKey
    {
        private const string algorithm = SecurityAlgorithms.HmacSha256;
        private const string key = "someSecretKey123!";

        /// <summary>
        /// Алгоритм шифирования.
        /// </summary>
        public string Algorithm => algorithm;

        /// <summary>
        /// Ключ цифровой подписи.
        /// </summary>
        public SymmetricSecurityKey SymmetricKey => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
    }
}
