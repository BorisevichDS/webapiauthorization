using NUnit.Framework;
using FakeItEasy;
using FluentAssertions;
using Core.Services;
using WebApi.Controllers;
using Core.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System;
using WebApi.Dtos;

namespace WebApiTests
{
    public class CustomersControllerTest
    {
        [Test]
        [Description("����� ������������� �������� ������ ���������� ��������� � ��� ������ ������ ���� Ok(200).")]
        public void CanGetWithOkCode()
        {
            // Arrange
            Customer customer = new Customer(1, "John Doe", "john_doe@mail.ru", "8(926)123-45-67");
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Returns(customer);

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Get(1).GetAwaiter().GetResult();
            var okResult = result.Result as ObjectResult;

            // Assert
            result.Should().NotBeNull();
            okResult.Value.Should().NotBeNull();
            okResult.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        [Description("����� �������������� �������� ������ ����������� � ����� ������ NotFound(404).")]
        public void CanGetWithNotFoundCode()
        {
            // Arrange
            Customer customer = null;
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Returns(customer);

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Get(1).GetAwaiter().GetResult();
            var notFoundResult = result.Result as StatusCodeResult;

            // Assert
            notFoundResult.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Test]
        [Description("����� �������� � ����������� ������ ����������� � ����� ������ BadGeteway(502).")]
        public void CanGetWithBadGatewayCode()
        {
            // Arrange
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Throws(new Exception());

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Get(1).GetAwaiter().GetResult();
            var notFoundResult = result.Result as StatusCodeResult;

            // Assert
            notFoundResult.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);
        }

        [Test]
        [Description("�������� �������� ������ ����������� � ����� ������ Ok(202).")]
        public void CanPostWithOkCode()
        {
            // Arrange
            Customer existingCustomer = null;
            CustomerDto newCustomer = new CustomerDto(1, "John Doe", "john_doe@mail.ru", "8(926)123-45-67");
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Returns(existingCustomer);

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Post(newCustomer).GetAwaiter().GetResult();
            var okResult = result.Result as StatusCodeResult;

            // Assert
            result.Should().NotBeNull();
            okResult.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        [Description("�������� ��������, ������� ��� ���������������, ������ ����������� � ����� ������ Conflict(409).")]
        public void CanPostWithConflictCode()
        {
            // Arrange
            Customer existingCustomer = new Customer(1, "John Doe", "john_doe@mail.ru", "8(926)123-45-67");
            CustomerDto newCustomer = new CustomerDto(1, "John Doe", "john_doe@mail.ru", "8(926)123-45-67");
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Returns(existingCustomer);

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Post(newCustomer).GetAwaiter().GetResult();
            var okResult = result.Result as StatusCodeResult;

            // Assert
            result.Should().NotBeNull();
            okResult.StatusCode.Should().Be((int)HttpStatusCode.Conflict);
        }

        [Test]
        [Description("�������� �������� � ����������� ������ ����������� � ����� ������ BadGeteway(502).")]
        public void CanPostWithBadGatewayCode()
        {
            // Arrange
            CustomerDto newCustomer = new CustomerDto(1, "John Doe", "john_doe@mail.ru", "8(926)123-45-67");
            IService<Customer, int> service = A.Fake<IService<Customer, int>>();

            A.CallTo(() => service.GetAsync(A<int>._)).Throws(new Exception());

            // Act
            CustomersController sut = new CustomersController(service);
            ActionResult<CustomerDto> result = sut.Post(newCustomer).GetAwaiter().GetResult();
            var okResult = result.Result as StatusCodeResult;

            // Assert
            result.Should().NotBeNull();
            okResult.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);

        }
    }
}